# vim: set fileencoding=utf8 :

import sys
from text_hr import *
import text_hr


def lemmatize(original, guessed_lemma, tag):
    try:
        if tag[0] == 'A':
            has_neo = tag[6] == 'n'
            try:
                word = Adjective(original, has_neo=has_neo, com_wp1_suff='iji')
            except Exception:
                word = Adjective(original[:-1] + 'i',
                                 has_neo=has_neo, com_wp1_suff='iji')
            lemma = word.forms['P_O/S/N/N'][0]
        elif tag[0] == 'N':
            try:
                word = Noun(original, tag[2].upper())
            except text_hr.morphs.BadParamsInitError:
                try:
                    word = Noun(guessed_lemma, tag[2].upper())
                except text_hr.morphs.BadParamsInitError:
                    try:
                        word = Noun(guessed_lemma[:-1], tag[2].upper())
                    except text_hr.morphs.BadParamsInitError:
                        return guessed_lemma
            lemma = word.forms['S/N'][0]
        elif tag[0] == 'V':
            try:
                word = Verb(original)
            except text_hr.morphs.BadParamsInitError:
                try:
                    word = Verb(guessed_lemma)
                except text_hr.morphs.BadParamsInitError:
                    try:
                        word = Verb(guessed_lemma, ext='ti')
                    except text_hr.morphs.BadParamsInitError:
                        return guessed_lemma

            lemma = word.forms['X_INF//'][0]
        else:
            lemma = original
    except Exception:
        return original

    return lemma


def preprocess(path):
    word_list = []

    with open(path) as f:
        for line in f:
            if not line.strip():
                continue

            original = unicode(line.split()[0], encoding='utf-8')
            guessed_lemma = unicode(line.split()[2], encoding='utf-8')
            tag = line.split()[1]
            word_list.append((original, guessed_lemma, tag))

    return word_list


def main():
    word_list = preprocess(sys.argv[1])
    for original, guessed_lemma, tag in word_list:
        lemma = unicode(lemmatize(original, guessed_lemma, tag))

        if guessed_lemma != lemma and original != lemma:
            print u"{}: {}\t{}\t{}".format(
                tag[0],
                original,
                guessed_lemma,
                lemma
            ).encode('utf-8')


if __name__ == '__main__':
    main()
