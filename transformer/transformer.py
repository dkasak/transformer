#!/usr/bin/python3

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

import os
import sys
import csv
from collections import Counter
from itertools import groupby
from typing import List, Tuple, Set, Dict

from .parser import build_parser
from .files import (get_last_file_num,
                    response_files,
                    process_path,
                    csv_data_files)
from .data import load_dataset, fas_by_rank
from .data_format import (responses_in_file,
                          responses_in_path,
                          transformed_with)
from .stats import stats_demographics
from .plot import plot_graph
from .misc import Ordering, levenshtein

module_path = os.path.dirname(os.path.realpath(__file__))
dictionary: Dict[str, List[str]] = {}
transformations = []


def transformed_by(rules: List[Tuple[str, str]],
                   expr: str,
                   path: str=None,
                   line: str=None) -> str:
    for search, replace in rules:
        try:
            if os.path.basename(path) == search[1:] and int(replace) == line:
                break
        except ValueError:
            pass

        if search.startswith('!') and expr == search[1:]:
            return expr
        elif search.startswith('?') and expr.lower() == search[1:].lower():
            return expr
        elif search.startswith('=') and expr == search[1:]:
            return replace
        elif search.startswith('~') and expr.lower() == search[1:].lower():
            return replace

        if expr.lower().endswith(search):
            return expr[:-len(search)] + replace

    return expr


# citaj popis specifikacija transformacija sa stdin i izvrši svaku
def transform(specs=sys.stdin) -> None:
    specs = [spec.strip() for spec in specs]

    grouped_by_file = groupby(specs, lambda spec: spec.split(':')[0])

    for path, file_specs in grouped_by_file:
        transformed_contents = transformed_with(path, file_specs)

        with open(path, 'w') as f:
            f.write(transformed_contents)


# Pretvori fileove iz online CSV formata u naš format (ili obrnuto).
# Input se čita iz input_path (može biti file ili direktorij), a sprema
# u output_path (treba biti file ili direktorij ovisno o modu).
def convert(mode: str, input_path: str, output_path: str) -> None:
    for f in csv_data_files(input_path):
        input_path = os.path.join(input_path, f)

        # ucitaj nove podatke iz input direktorija
        with open(input_path) as data_file:
            data_reader = csv.reader(data_file, delimiter=",")
            data = list(data_reader)

        # zapamti i odbaci header
        header = data[0]
        del data[0]

        # koji je rednji broj zadnje stvorenog filea u output direktoriju?
        last_num = get_last_file_num(output_path)

        # ispisi nove podatke, po jedan file za svakog ispitanika
        for broj, ispitanik in enumerate(data):
            filename = "{:03}".format(broj + last_num + 1)
            filepath = os.path.join(output_path, filename)
            output_file = open(filepath, "w")
            output_file.write(", ".join(ispitanik[1:6]) + "\n\n")
            data_writer = csv.writer(output_file, delimiter="\t")

            for i in zip(header[6:], ispitanik[6:]):
                stimulus = i[0].strip()
                response = i[1].strip()

                thing_to_write = (stimulus, response)

                if response == '?':
                    thing_to_write = (stimulus, '_')

                if response == '':
                    thing_to_write = (stimulus, '-')

                data_writer.writerow(thing_to_write)

            output_file.close()


# Računa nove odgovore u path2 (one kojih nema u path1)
def new_responses(path1: str, path2: str) -> Set[str]:
    return set(responses_in_path(path2)) - set(responses_in_path(path1))


# Pronalazi sve odgovore jednake stringu response u fileovima u dir i vraća
# listu tripleta koji sadrže:
#   1. file u kojem se odgovor nalazi
#   2. liniju u kojoj se nalazi
#   3. sam odgovor
def where_is(wanted_response, path, case_insensitive=False, suffix=False):
    if os.path.isdir(path):
        all_responses = responses_in_path(path, extra_info=True)
    else:
        all_responses = responses_in_file(path, extra_info=True)

    filtered_responses = []

    for path, line_num, stimulus, response, _ in all_responses:
        if case_insensitive:
            if suffix and response.lower().endswith(wanted_response.lower()) \
                    or wanted_response.lower() == response.lower():
                filtered_responses.append((path, line_num, stimulus, response))
        else:
            if suffix and response.endswith(wanted_response) \
                    or wanted_response == response:
                filtered_responses.append((path, line_num, stimulus, response))

    return filtered_responses


def rule_transform(file, rules):
    all_responses = responses_in_file(file, extra_info=True)
    result = []

    for path, line_num, _, response, _ in all_responses:
        result.append((path, line_num,
                       response, transformed_by(rules, response,
                                                path, line_num)))

    return result


def merge_fix(old_dir: str, new_dir: str, one_word_only: bool) -> List[str]:
    specs = []

    for path_old in response_files(old_dir):
        filename = os.path.basename(path_old)
        path_new = os.path.join(new_dir, filename)

        if not os.path.exists(path_new):
            print("File {} doesn't exist anymore in {}. "
                  "Skipping file.".format(filename, new_dir),
                  file=sys.stderr, flush=True)
            continue

        with open(path_old) as file_old:
            with open(path_new) as file_new:
                header_old = next(file_old)
                header_new = next(file_new)

                if header_old != header_new:
                    print("Warning: header mismatch between {} and {}. "
                          "Skipping file.".format(path_old, path_new),
                          file=sys.stderr, flush=True)
                    continue

                next(file_old)
                next(file_new)

                for line_num, (line_old, line_new) in \
                        enumerate(zip(file_old, file_new)):
                    items_old = line_old.strip().split('\t')
                    items_new = line_new.strip().split('\t')

                    stimulus_old = items_old[0]
                    stimulus_new = items_new[0]

                    response_old = items_old[1]
                    response_new = items_new[1]

                    if len(response_old.split()) > 1 and one_word_only:
                        continue

                    if stimulus_old != stimulus_new:
                        print("Warning: stimulus mismatch between "
                              "{} and {} on line {} ({} != {}). "
                              "Skipping line.".format(path_old,
                                                      path_new,
                                                      line_num,
                                                      stimulus_old,
                                                      stimulus_new),
                              file=sys.stderr, flush=True)
                        continue

                    # Ako odgovor sudionika u novom fileu nije isti kao odgovor
                    # sudionika u starom, nekad prije smo pogriješili
                    # i promijenili sudionikov odgovor (umjesto ispravili ga).
                    if response_old != response_new:

                        if (response_new[0] == '-' and
                            response_old == response_new[1:] or
                            response_old[0] == '-' and
                            response_old[1:] == response_new):
                            continue

                        # stvaramo transformaciju da ispravimo pogrešku
                        specs.append("{}:{}:=2{}:{}".format(path_new,
                                                            line_num + 3,
                                                            response_new,
                                                            response_old))

                        # Dodatno, ako u novom fileu ne postoji treća kolumna
                        # (ispravak), prebacujemo izmijenjeni ispitanikov
                        # odgovor u treću kolumnu (jer je to zapravo trebao
                        # biti ispravak). Ukoliko treća kolumna postoji, to
                        # znači da smo na krivom mjestu zapisan ispravak već
                        # naknadno opet mijenjali te tu promjenu uzimamo kao
                        # finalnu.
                        if len(items_new) == 2:
                            specs.append("{}:{}:=3:{}".format(path_new,
                                                              line_num + 3,
                                                              response_new))

    return specs


def init_dict() -> Dict[str, List[str]]:
    dicts = ['rivers',
             'locations',
             'names',
             'games',
             'entities',
             'abbreviations',
             'nationalities',
             'croatian-wordlist']

    d: Dict[str, List[str]] = {}

    for dictionary in dicts:
        with open(os.path.join(module_path, 'words', dictionary)) as f:
            for line in f:
                # skip comments
                if line.lstrip().startswith('#'):
                    continue

                entry = line.strip()
                d.setdefault(entry.lower(), []).append(entry)

    return d


def dictionary_fix_case(path: str) -> List[Tuple[str, int, str, str]]:
    global dictionary
    specs = []

    for file_path, line_num, _, response, _ in \
            responses_in_path(path, extra_info=True):
        normalized = response.lower()
        variants = dictionary.get(normalized)
        if variants is not None:
            canonical = variants[0]
            if response != canonical:
                specs.append((file_path, line_num, response, canonical))

    return specs


def fix_old_style_bans(path: str) -> List[str]:
    specs = []

    for file_path, line_num, _, response, column in \
            responses_in_path(path, extra_info=True, skip_bans=False):
        if response.startswith('-') and column == 1 and len(response) > 1:
            specs.append("{}:{}:{}:{}".format(file_path, line_num,
                                              response, '-'))
            specs.append("{}:{}:=2{}:{}".format(file_path, line_num,
                                                response, response[1:]))

    return specs


def ban(path: str, banned: str, case_sensitive: bool) -> \
        List[Tuple[str, int, str, str]]:

    specs = []

    for file_path, line_num, _, response, response_column in \
            responses_in_path(path, extra_info=True):
        compare = response

        if not case_sensitive:
            compare = response.lower()

        if compare in banned:
            if response_column == 2:
                specs.append((file_path,
                              line_num,
                              response,
                              '-{}'.format(response)))
            else:
                specs.append((file_path,
                              line_num,
                              response,
                              '-'.format(response)))

    return specs


def init():
    global transformations
    transformations.extend([e.strip().split('\t')
                      for e in open(os.path.join(module_path,
                                                 'rules',
                                                 'transformations'))
                      if not e.startswith('#')])


def main():
    init()
    parser = build_parser()
    args = parser.parse_args()

    try:
        if args.command in ['stimuli', 'cues']:
            data = load_dataset(args.paths)

            if args.count:
                counted_stimuli = data.groupby('cue', as_index=False).sum()
                counted_stimuli = counted_stimuli.sort_values(by=['freq', 'cue'])

                for stimulus, count in counted_stimuli.values:
                    print("{} {}".format(count, stimulus))
            else:
                stimuli = sorted(data['cue'].unique())

                for stimulus in stimuli:
                    print("{}".format(stimulus))
        elif args.command in ['responses', 'targets', 'associates']:
            responses = Counter()

            if args.edges:
                for path in args.paths:
                    responses.update(
                        (stimulus, response) for
                        _, _, stimulus, response, _ in
                        responses_in_path(path,
                                          uncorrected_only=args.uncorrected_only,
                                          extra_info=True))
            else:
                for path in args.paths:
                    responses.update(
                        responses_in_path(path,
                                          uncorrected_only=args.uncorrected_only,
                                          extra_info=args.extra_info))

            for response, count in responses.items():
                if args.num_words:
                    if ((args.num_words[0] == Ordering.GreaterOrEqual and
                            len(response.split()) < args.num_words[1]) or
                        (args.num_words[0] == Ordering.LessOrEqual and
                            len(response.split()) > args.num_words[1]) or
                        (args.num_words[0] == Ordering.Exact and
                            len(response.split()) != args.num_words[1])):

                        continue

                if args.frequency:
                    if ((args.frequency[0] == Ordering.GreaterOrEqual and
                            count < args.frequency[1]) or
                        (args.frequency[0] == Ordering.LessOrEqual and
                            count > args.frequency[1]) or
                        (args.frequency[0] == Ordering.Exact and
                            count != args.frequency[1])):

                        continue

                if args.edges:
                    print("{}\t{}\t{}".format(*response, count))
                else:
                    print(response)
        elif args.command == "analyze":
            df = load_dataset(args.paths)
            if args.mode == 'edges':
                df.to_csv(args.output)
            elif args.mode == 'fas_by_rank':
                fas_by_rank(df).to_csv(args.output)
        elif args.command == "where-is":
            for path in args.paths:
                responses = where_is(args.response,
                                     path,
                                     args.case_insensitive,
                                     args.suffix)

                for fname, line_num, stimulus, response in responses:
                    print("{}:{}:{}\t# {}".format(fname, line_num,
                                                  response, stimulus),
                          flush=True)
        elif args.command == "detect-discrepancies":

            if args.levenshtein:
                responses = set()

                for path in args.paths:
                    responses.update(
                        responses_in_path(path,
                                          uncorrected_only=True,
                                          extra_info=False))

                responses = sorted(responses)
                responses1 = responses[:-1]
                responses2 = responses[1:]

                for r1, r2 in zip(responses1, responses2):
                    l = levenshtein(r1, r2)

                    if ((args.levenshtein[0] == Ordering.GreaterOrEqual and
                            l < args.levenshtein[1]) or
                        (args.levenshtein[0] == Ordering.LessOrEqual and
                            l > args.levenshtein[1]) or
                        (args.levenshtein[0] == Ordering.Exact and
                            l != args.levenshtein[1])):

                        continue

                    print(r1, r2)
        elif args.command == "new-responses":
            new = new_responses(args.path1, args.path2)

            # ako zelimo samo odgovore s n ili vise rijeci
            if args.n != 0:
                filtered_new = set()

                for response in new:
                    # odgovor ima barem n rijeci
                    if len(response.split()) >= args.n:
                        filtered_new.add(response)

                new = filtered_new

            # ispisi odgovore samo ako skup odgovora nije duljine 0
            if len(new) > 0:
                if args.each_one_line:
                    for response in new:
                        print(response, flush=True)
                else:
                    print(new, flush=True)
        elif args.command == "replace":
            if args.case_insensitive:
                rule = [("~{}".format(args.original), args.replacement)]
            else:
                rule = [("={}".format(args.original), args.replacement)]

            for path in args.paths:
                process_path(rule_transform, path, rule)
        elif args.command == "ban":
            for path in args.paths:
                if args.response:
                    banned = [args.response
                              if args.case_sensitive
                              else args.response.lower()]
                else:
                    banned_file = os.path.join(module_path, "rules", "banned")

                    with open(banned_file) as f:
                        banned = f.read().split('\n')

                    if not args.case_sensitive:
                        banned = [b.lower() for b in banned]

                process_path(ban, path, banned, args.case_sensitive)
        elif args.command == "dictionary-fix-case":
            global dictionary
            dictionary = init_dict()
            for path in args.paths:
                process_path(dictionary_fix_case, path)
        elif args.command == "fix-old-style-bans":
            for path in args.paths:
                for f in fix_old_style_bans(path):
                    print(f)
        elif args.command == "merge-fix":
            for f in merge_fix(args.old_dir, args.new_dir, args.one_word_only):
                print(f)
        elif args.command in ("rule-transform", "croatian-stemmer-transform"):
            for path in args.paths:
                process_path(rule_transform, path, transformations)
        elif args.command == "stats-demographics":
            stats_demographics(args)
        elif args.command == "transform":
            transform()
        elif args.command == "convert":
            convert(args.input_dir, args.output_dir)
        elif args.command == "plot-graph":
            plot_graph(args)
        else:
            parser.print_help()
    except KeyboardInterrupt:
        sys.stderr.close()
        sys.exit(0)
    except BrokenPipeError as e:
        # ako pišemo u pipe i pipe bude zatvoren (jer je proces koji čita
        # završio), zatvorimo stderr i završavamo s izvršavanjem
        sys.stderr.close()
        sys.exit(0)
