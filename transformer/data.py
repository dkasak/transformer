#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

import pandas as pd
from collections import Counter
from typing import List, Union, cast

from .data_format import responses_in_paths


# "obviously-correct" way of calculating single mas for testing
def mas_single(df, cue, target):
    mas = 0
    for t in df[df.target_normed].loc[cue].index:
        fas1 = df.loc[cue, t].fas
        try:
            fas2 = df.loc[t, target].fas
            mas += fas1 * fas2
        except KeyError:
            pass
    return mas


# "obviously-correct" way of calculating single oas for testing
def oas_single(df, cue, target):
    oas = 0
    for t in df[df.target_normed].loc[cue].index:
        fas1 = df.loc[cue, t].fas
        try:
            fas2 = df.loc[target, t].fas
            oas += fas1 * fas2
        except KeyError:
            pass
    return oas


def oas(df):
    nf = df.reset_index()
    oas_data = (pd.merge(nf, nf, on='target')
                  .groupby(['cue_x', 'cue_y'])
                  .apply(lambda pair: (pair.fas_x * pair.fas_y).sum()))
    return oas_data


def mas(df):
    nf = df.reset_index()
    mas_data = (pd.merge(nf, nf, left_on='target', right_on='cue')
                  .groupby(['cue_x', 'target_y'])
                  .apply(lambda pair: (pair.fas_x * pair.fas_y).sum()))
    return mas_data


def load_dataset(paths: Union[str, List[str]]):
    # If the passed in path is actually a CSV file containing already
    # analyzed and compiled data, just load it in a DataFrame and return it
    if type(paths) is str:
        path = cast(str, paths)

        if path.endswith('.csv'):
            df = pd.read_csv(path)
            return df.set_index(['cue', 'target'])
    elif len(paths) == 1 and paths[0].endswith('.csv'):
        df = pd.read_csv(paths[0])
        return df.set_index(['cue', 'target'])

    # Build an edge (i.e. (cue, target) pair) list, along with their
    # frequencies.
    responses = Counter((cue, associate)
                        for _, _, cue, associate, _
                        in responses_in_paths(paths, extra_info=True))

    # Build a multi-level index with indices of form (cue, target).
    index = pd.MultiIndex.from_tuples(responses.keys(),
                                      names=['cue', 'target'])

    # Build a dataframe from the data, using index.
    df = pd.DataFrame(list(responses.values()), index=index, columns=['freq'])

    # Drop self-edges (i.e. edges of form A -> A)
    df = df.select(lambda x: x[0] != x[1])

    # Calculate set of cues
    cues = df.reset_index().cue.drop_duplicates()

    # Calculate FAS.
    fas = df.groupby('cue',
                     group_keys=False).apply(lambda g: g.freq / g.freq.sum())

    df = df.assign(fas=fas)

    # FAS(A, B) is BAS(B, A) so we swap cue and target in the index and do an
    # inner join.
    df_bas = df.filter(items=['fas'])
    df_bas = df_bas.swaplevel()
    df_bas.index.set_names(['cue', 'target'], inplace=True)
    df_bas.rename(columns={'fas': 'bas'}, inplace=True)
    df = df.join(df_bas)

    # Notably, when a target B in an (A, B) pair has been normed and the BAS of
    # the pair still hasn't been set, we approximate the BAS as being zero.
    zero_bas = cues.to_frame().assign(bas=0.0).set_index('cue')
    zero_bas.index.names = ['target']
    df = df.swaplevel().fillna(zero_bas).swaplevel()

    # Calculate number of each normed word
    n_associates = (df[lambda p: p.freq >= 2].groupby('cue')
                                             .apply(lambda d: len(d)))
    n_associates.name = 'n_associates'
    df = df.join(n_associates)

    # Calculate the number of times each cue was normed
    times_normed = df.groupby('cue').sum().freq
    times_normed.name = 'times_normed'
    df = df.join(times_normed)

    # Calculate whether the target of the edge is normed too
    normed_targets = cues.to_frame().set_index('cue') \
                         .assign(target_normed=True)
    normed_targets.index.name = 'target'
    df = df.reset_index(level='cue').join(normed_targets)
    df.target_normed.fillna(False, inplace=True)
    df = df.set_index('cue', append=True).swaplevel()

    df = df.assign(mas=mas(df))
    df = df.assign(oas=oas(df))

    return df.reindex_axis(['target_normed', 'freq',
                            'fas', 'bas', 'mas', 'oas',
                            'n_associates', 'times_normed'],
                           axis=1)


# Calculate average FAS by rank of the associate (out of all associates for
# a given cue) to which it belongs. In other words, calculates the average FAS
# of all the strongest associates of a word, and the average FAS of the second
# strongest associate and so on.
#
# Accepts a DataFrame of edges, produced by load_dataset.
def fas_by_rank(df):
    fas = df[['fas']]
    rank = fas.copy()
    fas_and_rank = fas.assign(
        rank=rank.groupby('cue')
                 .transform(lambda g: g.rank(ascending=False,
                                             method='first')))
    return fas_and_rank.groupby('rank').describe()
