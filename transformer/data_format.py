#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

# Functions for parsing and modifying the data in our data format

import sys
from collections import namedtuple
from typing import Set, Tuple, Union, List, Iterable, cast

from .files import response_files

Demographics = namedtuple('Demographics', ['gender', 'age', 'education',
                                           'population_size', 'county'])


def demographics_from_file(path):
    with open(path) as f:
        header = next(f).strip()
        (gender, age, education,
         population_size, county) = (x.strip() for x in header.split(','))
        return Demographics(gender, age, education, population_size, county)


# skup odgovora (ispravljenih ako postoje, inače ispitanikovih) u fileu
def responses_in_file(path,
                      uncorrected_only=False,
                      extra_info=False,
                      skip_bans=True):
    responses = set()
    f = open(path)

    # odbaci prve dvije linije
    next(f)
    next(f)

    for i, line in enumerate(f):
        line = line.strip()
        columns = line.split('\t')
        stimulus = columns[0]

        if len(columns) < 2:
            print("Warning: Line {} in file {} has no response. Skipping."
                  .format(i, path), file=sys.stderr, flush=True)
            continue

        if columns[1].startswith('-') and skip_bans or \
           columns[1].startswith('_') or \
           columns[1].startswith('#') or \
           len(columns) == 3 and columns[2].startswith('-') and skip_bans or \
           len(columns) == 3 and columns[2].startswith('_') and skip_bans:
            continue

        if len(columns) == 3:
            response = columns[2].strip()
            response_column = 2

            # skip responses with correction, if needed
            if uncorrected_only:
                continue

            if response == '':
                print("!!!", path, i, file=sys.stderr, flush=True)
                break
        else:
            response = columns[1].strip()
            response_column = 1

            if response == '':
                print("!!!", path, i, file=sys.stderr, flush=True)
                break

        if extra_info:
            responses.add((path, i + 3, stimulus, response, response_column))
        else:
            responses.add(response)

    return responses


# lista svih odgovora (ispravljenih ako postoje, inače ispitanikovih)
# u fileovima u paths (naš format)
def responses_in_paths(paths: List[str],
                       uncorrected_only: bool = False,
                       extra_info: bool = False,
                       skip_bans: bool = True) -> Union[
                           List[str],
                           List[Tuple[str, int, str, str, int]]
                       ]:

    # ugh, some mypy type wrangling
    total: Union[List[str], List[Tuple[str, int, str, str, int]]] \
        = cast(Union[List[str], List[Tuple[str, int, str, str, int]]], [])

    if extra_info:
        total = cast(List[Tuple[str, int, str, str, int]], total)
    else:
        total = cast(List[str], total)

    for path in paths:
        for file_path in response_files(path):
            # izračunaj odgovore u fileu
            responses = responses_in_file(file_path,
                                          uncorrected_only,
                                          extra_info,
                                          skip_bans)
            if extra_info:
                responses = cast(Set[Tuple[str, int, str, str, int]],
                                 responses)
            else:
                responses = cast(Set[str], responses)

            # dodaj ih u listu
            total.extend(responses)

    return total


# isto kao responses_in_paths, ali za samo jedan path
def responses_in_path(path: str,
                      uncorrected_only: bool = False,
                      extra_info: bool = False,
                      skip_bans: bool = True) -> \
        Union[List[str], List[Tuple[str, int, str, str, int]]]:

    return responses_in_paths([path],
                              uncorrected_only=uncorrected_only,
                              extra_info=extra_info,
                              skip_bans=skip_bans)


# konstruiraj izmijenjeni sadržaj file-a na putanji path na način da se na
# njegov sadržaj primijene sve transformacije file_specs
def transformed_with(path: str, specs: Iterable[str]) -> str:
    contents: List[List[str]] = []

    with open(path) as f:
        # odbaci prve dvije linije, spremajući header
        header = next(f)
        next(f)

        # dodaj sve ostale linije, splittane u kolumne
        contents.extend(line.strip().split('\t') for line in f)

    def get_response_and_column(line):
        if len(line) == 3:
            return line[2], 2
        else:
            try:
                return line[1], 1
            except IndexError:
                print("Tab error in file {}, line {}".format(path, line_num),
                      file=sys.stderr, flush=True)

    # izvrsi transformacije
    for spec_str in specs:
        spec = spec_str.split(':')
        line_num = int(spec[1])
        response = spec[2]
        replacement = spec[3]
        index = line_num - 3

        # ako stvar koju treba zamijeniti započinje s = metaznakom (iza kojeg
        # je broj), to je signal da smo u modu transformacije određene kolumne,
        # a ne modu ispravka ispitanikovog odgovora
        if response.startswith('='):
            try:
                column = int(response[1])
            except ValueError:
                print("Invalid spec (= not followed by number). Skipping.",
                      file=sys.stderr, flush=True)
                continue

            response = response[2:]

            if response == contents[index][column-1]:
                if column == 3 and response == '':
                    contents[index].append(replacement)
                else:
                    contents[index][column-1] = replacement
            else:
                original = contents[index][column-1]
                msg = "Warning: mismatch in response in {}:{} ({} != {})." \
                      " Skipping.".format(path, line_num, original, response)
                print(msg, file=sys.stderr, flush=True)
        else:
            original, response_column = \
                get_response_and_column(contents[index])
            if original == response:
                if response_column == 1 and replacement != original:
                    contents[index].append(replacement)
                elif response_column == 2:
                    contents[index][response_column] = replacement
            else:
                msg = "Warning: mismatch in response in {}:{} ({} != {})." \
                      " Skipping.".format(path, line_num, original, response)
                print(msg, file=sys.stderr, flush=True)

    new_contents = (header + "\n" +
                    "\n".join("\t".join(line) for line in contents) +
                    "\n")

    return new_contents
