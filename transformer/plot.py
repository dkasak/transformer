#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

import igraph
import colorsys
from math import pi

from .data import load_dataset
from .graph import graph_from_data


def gradient(t):
    r = 226 / 255
    g = 29 / 255
    b = 29 / 255

    h, _, s = colorsys.rgb_to_hls(r, g, b)
    l = min(0.9, 0.5 + (0.85 - t**0.5))
    r, g, b = colorsys.hls_to_rgb(h, l, s)

    return (r, g, b)


def plot_graph(args):
    label_color = '#186B8E'
    normed_color = '#B20D0D'
    unnormed_color = '#347896'

    if args.quality == 'fast':
        iters = 500
    elif args.quality == 'medium':
        iters = 1000
    elif args.quality == 'best':
        iters = 5000
    else:
        # can't happen
        assert False

    data = load_dataset(args.paths)
    g = graph_from_data(data, args)

    g.vs['label_size'] = 17
    g.vs['label_dist'] = 3
    g.vs['label_angle'] = 1/3 * pi
    g.vs['label_color'] = label_color
    g.vs['size'] = 24
    g.vs['color'] = [normed_color
                     if v['normed']
                     else unnormed_color
                     for v in g.vs]

    style = {}
    style['edge_width'] = [w * 12 for w in g.es['weight']]
    style['edge_color'] = [gradient(w) for w in g.es['weight']]
    style['margin'] = 150

    if not args.giant:
        num_vertices = len(g.vs)
        bbox_x = 1300 + 2000 * num_vertices / 600
        bbox_y = 1300 + 2000 * num_vertices / 600
        style['bbox'] = (bbox_x, bbox_y)

        layout = g.layout('fr',
                          weights='weight')
                          # maxiter=iters,
                          # coolexp=1.6)
        igraph.plot(g,
                    args.output_file,
                    layout=layout,
                    **style)
    else:
        clustering = g.clusters(mode=args.giant.upper())
        g = clustering.giant()

        num_vertices = len(g.vs)
        bbox_x = 1300 + 2000 * num_vertices / 600
        bbox_y = 1300 + 2000 * num_vertices / 600
        style['bbox'] = (bbox_x, bbox_y)

        layout = g.layout('fr')

        igraph.plot(g,
                    args.output_file,
                    layout=layout,
                    **style)
