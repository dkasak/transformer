#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

from collections import Counter
from ascii_graph import Pyasciigraph as PrettyGraph
import pandas as pd

from .data_format import demographics_from_file
from .files import response_files
from .misc import histogram

age_groups = [range(18, 28),
              range(28, 38),
              range(38, 48),
              range(48, 58),
              range(58, 68),
              range(68, 78),
              range(78, 88)]


def age_to_group(age_str):
    try:
        age = int(age_str)
        group = age_groups[[age in g for g in age_groups].index(True)]
        return '{}-{}'.format(group.start, group.stop - 1)
    except ValueError:
        return age_str


def group_by_age_groups(ages):
    c = Counter()

    for age, count in ages.items():
        c.update({age_to_group(age): count})

    return c


def stats_demographics(args):
    gender = Counter()
    age = Counter()
    education = Counter()
    population_size = Counter()
    county = Counter()

    for path in args.paths:
        for data_file in response_files(path):
            demo = demographics_from_file(data_file)

            gender[demo.gender] += 1
            age[demo.age] += 1
            education[demo.education] += 1
            population_size[demo.population_size] += 1
            county[demo.county] += 1

    if args.age_groups:
        age = group_by_age_groups(age)

    def print_histogram(name, data, ascii_only=False):
        if ascii_only:
            print(name)
            print('=' * len(name))
            print(histogram(data))
        else:
            graph = PrettyGraph()
            for line in graph.graph(name, sorted(data.items())):
                print(line)

        print()

    print_histogram('Spol', gender, args.ascii)
    print_histogram('Dob', age, args.ascii)
    if not args.age_groups:
        age_values = [int(a) for a in age.elements() if a.isdigit()]
        print(pd.Series(age_values).describe(), end='\n\n')
    print_histogram('Obrazovanje', education, args.ascii)
    print_histogram('Veličina mjesta', population_size, args.ascii)
    print_histogram('Županija', county, args.ascii)
