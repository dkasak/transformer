#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

from enum import Enum
from typing import Iterable, Dict, Tuple
from itertools import chain
import os


def expand_path(path: str) -> Iterable[str]:
    if os.path.isdir(path):
        for item in os.listdir(path):
            # preskoci skrivene fileove (kojima ime pocinje s .)
            if os.path.basename(item).startswith('.'):
                continue
            elif os.path.isdir(item):
                continue

            yield os.path.join(path, item)
    else:
        yield path


def expand_paths(paths: Iterable[str]) -> Iterable[str]:
    yield from chain(*(expand_path(path) for path in paths))


def histogram(data: Dict[str, int]) -> str:
    rows = []
    max_stars = 50
    max_val = max(data.values())

    for key, val in sorted(data.items()):
        stars = int(val / max_val * max_stars)
        spaces = max_stars - stars if max_stars - stars > 0 else 0
        histogram_string = '*' * stars + ' ' * spaces
        rows.append("{stars}\t {num:>3}  {value}"
                    .format(value=key,
                            stars=histogram_string,
                            num=val))

    return '\n'.join(rows)


def levenshtein(source: str, target: str) -> int:
    import numpy as np
    if len(source) < len(target):
        return levenshtein(target, source)

    # So now we have len(source) >= len(target).
    if len(target) == 0:
        return len(source)

    # We call tuple() to force strings to be used as sequences
    # ('c', 'a', 't', 's') - numpy uses them as values by default.
    source_array = np.array(tuple(source))
    target_array = np.array(tuple(target))

    # We use a dynamic programming algorithm, but with the
    # added optimization that we only need the last two rows
    # of the matrix.
    previous_row = np.arange(target_array.size + 1)
    for s in source_array:
        # Insertion (target_array grows longer than source_array):
        current_row = previous_row + 1

        # Substitution or matching:
        # target_array and source_array items are aligned, and either
        # are different (cost of 1), or are the same (cost of 0).
        current_row[1:] = np.minimum(
            current_row[1:],
            np.add(previous_row[:-1], target_array != s))

        # Deletion (target_array grows shorter than source_array):
        current_row[1:] = np.minimum(
            current_row[1:],
            current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]


class Ordering(Enum):
    Exact = 0
    LessOrEqual = 1
    GreaterOrEqual = 2


def num_range(num_str: str) -> Tuple[Ordering, int]:
    """ Utility function that parses strings of the form

            [+|-]<number>

        and returns a (Ordering, int) tuple representing the selected range of
        numbers.
    """

    if num_str[0] == '+':
        return (Ordering.GreaterOrEqual, int(num_str[1:]))
    elif num_str[0] == '-':
        return (Ordering.LessOrEqual, int(num_str[1:]))
    else:
        return (Ordering.Exact, int(num_str))
