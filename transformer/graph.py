#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

import igraph

from .data import load_dataset

defaults = {
    'min_freq': None,
    'max_freq': None,
    'min_fas': None,
    'max_fas': None,
    'cues_of': None,
    'associates_of': None,
}


def mock(**attrs):
    r = lambda: 0
    r.__dict__ = attrs
    return r


def graph_from_data(data, args):
    d = defaults.copy()
    # import pdb; pdb.set_trace()
    d.update(args.__dict__)
    args = mock(**d)

    responses = data.reset_index()

    g = igraph.Graph().as_directed()
    vertices = set()

    if args.cues_of or args.associates_of:
        responses = responses[responses.cue.isin(args.associates_of) |
                              responses.target.isin(args.cues_of)]

    normed = responses.cue.unique()

    if args.min_freq:
        responses = responses[responses.freq >= args.min_freq]

    if args.max_freq:
        responses = responses[responses.freq <= args.max_freq]

    if args.min_fas:
        responses = responses[responses.fas >= args.min_fas]

    if args.max_fas:
        responses = responses[responses.max <= args.max_fas]

    cues = responses.cue.unique()
    targets = responses.target.unique()
    vertices.update(cues, targets)
    edges = responses[['cue', 'target', 'fas']].values

    g.add_vertices(list(vertices))

    weights = [f for _, _, f in edges]

    g.add_edges((s, t) for s, t, _ in edges)
    g.vs['label'] = g.vs['name']
    g.vs['normed'] = [v in normed for v in g.vs['name']]
    g.es['weight'] = weights

    return g
