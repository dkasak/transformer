#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

import os
from typing import Iterable

from .misc import expand_path


# popis svih *.csv fileova u path (online format)
def csv_data_files(path: str) -> Iterable[str]:
    for path in expand_path(path):
        if path.endswith('.csv') and not path.startswith('.'):
            yield path


# najveći redni broj filea u direktoriju d (naš format)
def get_last_file_num(d: str) -> int:
    nums = []

    for f in os.listdir(d):
        if f.isdigit():
            nums.append(int(f))

    return max(nums) if len(nums) != 0 else 0


# Vraca listu imena fileova s odgovorima.
# Argument moze biti ime direktorija, u kojem slucaju ce lista sadrzavati imena
# svih fileova s podacima unutar tog direktorija, ili ime filea, u kojem
# slucaju ce lista sadrzavati samo ime tog jednog filea.
# Imena svih fileova s podacima se moraju sastojati samo od brojeva.
def response_files(path: str) -> Iterable[str]:
    for path in expand_path(path):
        # preskoci fileove kojima filenames nisu brojevi
        try:
            int(os.path.basename(path))
        except ValueError:
            continue

        yield path


def process_file(transformation, file_path, *args):
    for path, line_num, orig, trans in transformation(file_path, *args):
        if orig != trans:
            print("{}:{}:{}:{}".format(path, line_num, orig, trans),
                  flush=True)


def process_path(transformation, path, *args):
    if os.path.isdir(path):
        for fname in os.listdir(path):
            file_path = os.path.join(path, fname)

            # skip all non-files
            if not os.path.isfile(file_path):
                continue

            # preskoci fileove kojima filenames nisu brojevi
            try:
                int(fname)
            except ValueError:
                continue

            process_file(transformation, file_path, *args)
    else:
        process_file(transformation, path, *args)
