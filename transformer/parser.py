#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Copyright 2017 Denis Kasak <dkasak@termina.org.uk>
#  Copyright 2017 Nika Vezmar <nvezmar@termina.org.uk>

import argparse
import sys

from .misc import num_range


def build_parser():
    parser = argparse.ArgumentParser(prog="transformer")
    subparsers = parser.add_subparsers(dest="command")

    # stimuli komanda
    parser_stimuli = subparsers.add_parser(
        "stimuli",
        aliases=['cues'],
        help="output set of stimuli")
    parser_stimuli.add_argument(
        "-c", "--count",
        action="store_true",
        help="also output number of times a stimulus has been normed")
    parser_stimuli.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="file or directory with data to process")

    # responses komanda
    parser_responses = subparsers.add_parser(
        "responses",
        aliases=['associates', 'targets'],
        help="analyze data and output set/list of responses")
    parser_responses.add_argument("paths",
                                  metavar="PATH",
                                  nargs='+',
                                  help="path(s) with response data")
    parser_responses.add_argument("-e", "--extra-info",
                                  action='store_true',
                                  help="include extra info "
                                       "(file, line number, stimulus)")
    parser_responses.add_argument("--edges",
                                  action='store_true',
                                  help="output list of edges "
                                       "(stimulus, response, frequency)")
    parser_responses.add_argument("-o", "--uncorrected-only",
                                  action='store_true',
                                  help="output responses without "
                                       "correction only")
    parser_responses.add_argument(
        "-n", "--num-words",
        type=num_range,
        metavar='N',
        default=0,
        help="number of words in a response (use -N or +N to specify "
             "less-or-equal and greater-or-equal, respectively")
    parser_responses.add_argument(
        "-f", "--frequency",
        type=num_range,
        metavar='N',
        default=0,
        help="frequency of a response (use -N or +N to specify "
             "less-or-equal and greater-or-equal, respectively")

    # analyze komanda
    parser_analyze = subparsers.add_parser(
        "analyze",
        help="load raw experimental data and analyze it, "
             "producing a csv file with the results; "
             "supports multiple analysis modes")
    parser_analyze.add_argument(
        "-o", "--output",
        default=sys.stdout,
        required=False,
        help="path to csv file in which to save the data")
    parser_analyze.add_argument(
        "--mode",
        default='edges',
        choices=['edges', 'fas_by_rank'],
        help="what analysis to perform")
    parser_analyze.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="file or directory with data to process")

    # where-is komanda
    parser_where_is = subparsers.add_parser(
        "where-is",
        help="prints file(s) and line(s) in which a particular response has "
             "been given")
    parser_where_is.add_argument("-i", "--case-insensitive",
                                 action="store_true",
                                 help="make the match case-insensitive")
    parser_where_is.add_argument("-s", "--suffix",
                                 action="store_true",
                                 help="search for a suffix instead of a whole "
                                      "response")
    parser_where_is.add_argument("response",
                                 help="response to search for")
    parser_where_is.add_argument("paths",
                                 nargs='+',
                                 metavar="PATH",
                                 help="file or directory to search")


    # detect-discrepancies komanda
    parser_detect_discrepancies = subparsers.add_parser(
        "detect-discrepancies",
        help="analyze the dataset for discrepancies based on various methods")
    parser_detect_discrepancies.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="path(s) with responses")
    parser_detect_discrepancies.add_argument(
        "-l", "--levenshtein",
        metavar='N',
        type=num_range,
        help="output lexicographically adjacent words that are "
             "Levenshtein-separated by (at most/more than/exactly) N")

    # new-responses komanda
    parser_new_responses = subparsers.add_parser(
        "new-responses",
        help="computes new responses in a directory (compared to an old"
             " directory)")
    parser_new_responses.add_argument(
        "path1",
        metavar="PATH1",
        help="file or directory with old data")
    parser_new_responses.add_argument(
        "path2",
        metavar="PATH2",
        help="file or directory with new data")
    parser_new_responses.add_argument(
        "-1",
        action="store_true",
        dest="each_one_line",
        help="print each response in its own line")
    parser_new_responses.add_argument(
        "-n",
        metavar="N",
        type=int,
        default=0,
        help="minimum number of words in a response")

    # replace komanda
    parser_replace = subparsers.add_parser(
        "replace",
        help="replaces all occurence of a response with a correction")
    parser_replace.add_argument("-i", "--case-insensitive",
                                action='store_true',
                                help="whether to consider response to replace "
                                     "as case insensitive")
    parser_replace.add_argument("original",
                                help="response to replace")
    parser_replace.add_argument("replacement",
                                help="what to replace the response with")
    parser_replace.add_argument("paths",
                                metavar='PATH',
                                nargs='+',
                                help="path to replace response in")

    # ban komanda
    parser_ban = subparsers.add_parser(
        "ban",
        help="ban all occurences of some responses; bans either responses "
             "contained in the file 'rules/banned' or a response specified on "
             "the command line")
    parser_ban.add_argument(
        "-r", "--response",
        required=False,
        default=None,
        help="response to ban")
    parser_ban.add_argument(
        "-s", "--case-sensitive",
        action="store_true",
        help="match responses to ban case-sensitively")
    parser_ban.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="input file with responses to transform or a dir from which to"
             " take such input files")

    # dictionary-fix-case komanda
    parser_dictionary_fix_case = subparsers.add_parser(
        "dictionary-fix-case",
        help="fix case of responses by comparing to entries in user-supplied"
             " dictionaries (put the dictionary under the words/ directory,"
             " one entry per line)")
    parser_dictionary_fix_case.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="input file with responses to transform or a dir from which to"
             " take such input files")

    # fix-old-style-bans komanda
    parser_fix_old_style_bans = subparsers.add_parser(
        "fix-old-style-bans",
        help="fix old style bans with the dash in the second column, moving it"
             " to the third")
    parser_fix_old_style_bans.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="input file with responses to transform or a dir from which to"
             " take such input files")

    # merge-fix komanda
    parser_new_responses = subparsers.add_parser(
        "merge-fix",
        help="merges two directories with responses, old and new, ensuring no"
             " participant responses were changed in the new directory"
             " compared to the old one; participant responses should always be"
             " corrected (third column), never changed (second column)")
    parser_new_responses.add_argument(
        "old_dir",
        help="directory with old data (to compare against)")
    parser_new_responses.add_argument(
        "new_dir",
        help="directory with new data (that needs correction)")
    parser_new_responses.add_argument(
        "--one-word-only",
        action="store_true",
        help="only output corrections for responses which consist of one word"
             "only")

    # rule-transform (croatian-stemmer-transform) komanda
    parser_rule_transform = subparsers.add_parser(
        "rule-transform",
        aliases=["croatian-stemmer-transform"],
        help="transforms responses according to rules in "
             "rules/transformations")
    parser_rule_transform.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="input file with responses to transform or a dir from which to"
             " take such input files")

    # stat-demographics komanda
    parser_demographics = subparsers.add_parser(
        "stats-demographics",
        help="output demographics")
    parser_demographics.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="file or directory with data to process")
    parser_demographics.add_argument(
        '-a', '--ascii',
        action='store_true',
        help="print the graph using ASCII asterisks")
    parser_demographics.add_argument(
        '-g', '--age-groups',
        action='store_true',
        help="bucket age into age groups")

    # transform komanda
    parser_transform = subparsers.add_parser(
        "transform",
        help="read transformation specifications from stdin and execute them")

    # convert komanda
    parser_convert = subparsers.add_parser(
        "convert",
        help="convert data from the new format into the old format")
    parser_convert.add_argument(
        "-i", "--input-dir",
        metavar="DIR",
        required=True,
        help="directory with data files in new format")
    parser_convert.add_argument(
        "-o", "--output-dir",
        metavar="DIR",
        required=True,
        help="directory where data files in old format will be written to")

    # plot-graph komanda
    parser_plot_graph = subparsers.add_parser(
        "plot-graph",
        help="plot association graph")
    parser_plot_graph.add_argument(
        "paths",
        nargs='+',
        metavar="PATH",
        help="path(s) with raw responses or CSV containing anaylzed data")
    parser_plot_graph.add_argument(
        "-o", "--output-file",
        metavar="FILE",
        help="filename to save the plot as")
    parser_plot_graph.add_argument(
        "--min-freq",
        type=int,
        default=None,
        help="minimum edge frequency to plot (inclusive)")
    parser_plot_graph.add_argument(
        "--max-freq",
        type=int,
        default=None,
        help="maximum edge frequency to plot (inclusive)")
    parser_plot_graph.add_argument(
        "--min-fas",
        type=float,
        default=None,
        help="minimum FAS required to plot an edge")
    parser_plot_graph.add_argument(
        "--max-fas",
        type=float,
        default=None,
        help="maximum FAS required to plot an edge")
    parser_plot_graph.add_argument(
        "--cues-of",
        default=[],
        action='append',
        metavar='ASSOCIATE',
        help="plot only the supplied associate and its cues")
    parser_plot_graph.add_argument(
        "--associates-of",
        default=[],
        action='append',
        metavar='CUE',
        help="plot only the supplied cue and its associates")
    parser_plot_graph.add_argument(
        "--giant",
        default=None,
        choices=['weak', 'strong'],
        help="plot only the giant connected componented (either strongly or weakly connected)")
    parser_plot_graph.add_argument(
        "-q", "--quality",
        choices=['fast', 'medium', 'best'],
        default='medium',
        help="quality of the layout algorithm; fast is worst, best is slowest")

    return parser
