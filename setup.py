from os import path
from setuptools import setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
# with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
#     long_description = f.read()

setup(
    name='transformer',
    version='1.0.0',
    description='A data wrangling and association tool for free association experiments',
    url='https://gitlab.com/dkasak/transformer',
    author='Denis Kasak, Nika Vezmar',
    author_email='dkasak@termina.org.uk, nvezmar@termina.org.uk',
    license='GPL',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPL License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='language science data',
    packages=['transformer'],

    install_requires=['ascii-graph', 'python-igraph', 'pandas', 'numpy', 'pycairo'],
    package_data={
        'transformer': ['rules', 'words', 'rules/*', 'words/*'],
    },
    entry_points={
        'console_scripts': [
            'transformer=transformer.transformer:main',
        ],
    },
)
